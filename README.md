These are a few perl scripts that are useful to me.
A few that you may find useful are:

* `mkbooklet.pl` and `pscenter.pl`: Scripts to convert a PDF into a eco-friendly 2up document zooming in on the actual text.

* `backup.pl` Efficient, incremental backups with `rsync`
