#!/usr/bin/perl -w

use strict;

while(<STDIN>) {
  chomp;
  @_ = split /\t/;
  print "alias $_[0]\t$_[1] <$_[2]>\n";
} 
