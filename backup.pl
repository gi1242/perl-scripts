#! /usr/bin/perl -w
# Created   : Thu 13 Jun 2019 10:31:15 AM EDT
# Modified  : Tue 16 Jul 2019 04:40:14 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'

use strict;
use Term::ANSIColor qw(:constants);
use List::Util qw(any min);

my ($BD,$UL,$IT,$ER,$RE)=(RESET.CYAN,RESET.GREEN,RESET.YELLOW,RESET.RED,RESET);

my $backup_dir='/mnt/raid-store/backup';
my @excludes = qw(
    *.aux *.blg *.brf *.cb *.dvi *.fdb_latexmk *.fls *.fmt *.log
    *.out *.pdfsync *.toc .*.pdf.zpid
    *.plc *.pmc *.pyc
    *.o core
    .*.un~ .*.swp .*.swo
    .FvwmConsole-Socket FvwmCommand-*
    *.bak *.orig* *~
    *.pyo *.pyc .ipynb_checkpoints/ __pycache__/ Untitled*.ipynb
    *.wdiff
    .stversions/
    .DS_Store* ._* .Spotlight-V100 .Trashes Icon Ico thumbs.db Thumbs.db
    .dropbox
    etc/backup_state/ tmp/ /net/ /Downloads/ /Documents/ /Videos/ /Pictures/);
my @rsync_args = qw(-xa -HAX --delete-after --fuzzy --fuzzy
    --delete-excluded);

my $epoc = time();
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();

my ($src, $dst) = (shift, shift);
if( !defined( $src ) or !defined( $dst) )
{
    exec( qw(perldoc -T -o term), $0 );
    die();
}
$src =~ s:/$::;
$dst =~ s:/$::;

foreach my $p (@excludes)
{
    push( @rsync_args, "--exclude=$p" );
}
push( @rsync_args, @ARGV ) if( scalar(@ARGV) and (shift eq '--') );

backup( $src, $dst );

sub backup
{
    my ($src, $dst) = (shift, shift);
    my ($host, $dst_dir, @prefix);

    my (@extra_args, @backups, @keeps);
    my ($time, $y, $m, $d);

    $time = time();
    ($y, $m, $d) = ymd($time);
    
    ($host, $dst_dir) =
	( $dst =~ m/^(.+):(.+)$/ ) ? ($1, $2) : ('', $dst);
    @prefix = $host ? ('ssh', $host) : ();

    run( @prefix, qw(mkdir -p), $dst_dir );
    @backups=sort {$b cmp $a}
	grep( m/^20\d\d-[01]\d-[0-3]\d$/ma,
	    (qx(@prefix ls -1 $dst_dir)) );
    chomp( @backups );

    for my $p (@backups)
    {
	push( @extra_args, "--link-dest=../$p/")
	    if( $p ne "$y-$m-$d" );
	    #
	# at most 20 --link-dest opts are accepted
	last if( scalar(@extra_args) >= 20);
    }
    run( 'rsync', @rsync_args, @extra_args, "$src/", "$dst/$y-$m-$d/" );

    # Decide which backups to keep. ( Daily first week, weekly next 4 months,
    # monthly next 2 years)
    for( my $i=0; $i <= 7; $i++ )
    {
	my ($y, $m, $d) = ymd($time - $i*24*60*60);
	push( @keeps, "$y-$m-$d" );
    }

    for( my $i=0; $i <= 24; $i++)
    {
	my $bm = ($m - 1 - $i) % 12 + 1;
	my $by = int( ($y * 12 + $m - 1 - $i) / 12 );
	push( @keeps, sprintf( "$by-%02d-01", $bm ) );
	if( $i <= 3 )
	{
	    push( @keeps, sprintf( "$by-%02d-08", $bm ) );
	    push( @keeps, sprintf( "$by-%02d-15", $bm ) );
	    push( @keeps, sprintf( "$by-%02d-22", $bm ) );
	}
    }

    # Now remove the old backups
    for my $backup_dir (@backups)
    {
	run( @prefix, qw(rm -rf), "$dst_dir/$backup_dir" )
	    unless( any { $_ eq $backup_dir } @keeps );
    }
}

sub run
{
    print( $BD, 'Running ', join(' ', @_), "$RE\n" );
    system( @_ );
    #system( 'echo', @_ ); # For debugging
}

sub ymd
{
    my $time = shift;
    $time = time() if( !defined( $time ) );

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
	= localtime( $time );

    return (1900+$year, sprintf('%02d', $mon+1),
	sprintf('%02d', $mday) );
}

# POD documentation
=head1 NAME

B<backup> I<src> I<dst> I<[-- rsync options]>

=head1 DESCRIPTION

Backups I<src> onto I<dst>, keeping hardlinked snapshots of previous backups.
I<dst> can be a remote host. Backups are done via B<rsync>, and are stored in
I<dst> in folders named by the date. This script is just a wrapper to execute
something like:

    mkdir -p dst
    rsync --link-dest=../2019-06-27 ... --fuzzy src dst/2019-06-28
    rm -rf (old enough backups)

taking care that I<dst> might be a local or remote folder.

The backups that are B<kept> are:

=over

=item * One a day for a week.

=item * One a week for 4 months.

=item * One a month for 2 years.

=back

Everything else is removed.

By default a few temporary output files are excluded by passing I<--exclude>
options to B<rsync>. You can add more by passing an I<--exclude-from> or
I<--filter-from> option to B<rsync>. (The syntax is flexible enough that you
can clear current excluded files as well.)
