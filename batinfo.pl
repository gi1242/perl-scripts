#! /usr/bin/perl -w
# Print CPU temperature and battery state

use strict;

my $line;
my ($cpu_temp, $bat_capacity_max, $bat_capacity_state, $bat_charging_state,
    $bat_capacity);

# Get CPU temperature
open( CPUTEMP, '/proc/acpi/thermal_zone/THRM/temperature' )
    or die( "Could not read CPU temperature\n" );
chomp( $cpu_temp = <CPUTEMP> );
$cpu_temp =~ s/^temperature:\s+([0-9.]+)\s+(.*)/$1$2/;
close( CPUTEMP );

# Get battery state and level.
open( BATINFO, '/proc/acpi/battery/BAT0/info' )
    or die( "Could not read battery info\n" );
while( $line = <BATINFO> )
{
    if( $line =~ m/^last full capacity:\s+([0-9.]+)/ )
    {
	$bat_capacity_max = $1;
	last;
    }
}
close( CPUTEMP );

open( BATSTATE, '/proc/acpi/battery/BAT0/state' )
    or die( "Could not read battery state\n" );
while( $line = <BATSTATE> )
{
    if( $line =~ m/^capacity state:\s+(.*)$/ )
    {
	$bat_capacity_state = $1;
    }
    elsif( $line =~ m/^charging state:\s+(.*)$/ )
    {
	$bat_charging_state = $1;
    }
    elsif( $line =~ m/^remaining capacity:\s+([0-9.]+)/ )
    {
	$bat_capacity = $1;
    }
}

#die( "Did not read complete battery info\n" )
#    if( !defined( $bat_capacity_state ) || !defined( $bat_charging_state )
#	|| !defined( $bat_capacity ) || !defined( $bat_capacity_max ) );

printf( "CPU: %s. Battery %3.0f%% %s (state: %s).\n",
    $cpu_temp, $bat_capacity / $bat_capacity_max * 100, $bat_charging_state,
    $bat_capacity_state);
