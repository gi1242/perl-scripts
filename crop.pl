#! /usr/bin/perl -w
# Crop photos to a certain aspect ratio

use strict;
use Image::Magick;
use Getopt::Long qw(:config no_ignore_case bundling);

sub print_usage
{
    die( "Bad usage\n" );
}

#
# Options
#
my $ar = 1.5;		# Aspect ratio to crop to
my $height = undef;	# Height of the new image
my $width = undef;	# Width of the new image

#
# Variables
#
my ($imgwidth, $imgheight);

# Process command line options
GetOptions(
    "aspect-ratio|a=f"	=> \$ar,
    "width|w=i"		=> \$width,
    "height|h=i"	=> \$height
    #"output|o=s"	=> \$output
) or print_usage();

my $infile = shift;
my $outfile = shift;

my $image = Image::Magick->new();

die( "Error reading $_[1]\n" )
    if( $image->Read( $infile ) );

# Get the image width and height
($imgwidth, $imgheight) = $image->Get( 'columns', 'rows' );

if( $imgwidth < $imgheight )
{
    $image->Rotate( degrees=>90 );
    ($imgwidth, $imgheight) = ($imgheight, $imgwidth);
}

if( $imgwidth / $imgheight > $ar )
{
    my $newwidth = $imgheight * $ar;
    my $x = ($imgwidth - $newwidth) / 2;

    $image->Crop( geometry=>"${newwidth}x$imgheight+$x+0" );
}
else
{
    my $newheight = $imgwidth / $ar;
    my $y = ($imgheight - $newheight) / 2;

    $image->Crop( geometry=>"${imgwidth}x$newheight+0+$y" );
}

# Get new dimensions of the image in imgwidth, imgheight.
($imgwidth, $imgheight) = $image->Get( 'columns', 'rows' );

# Set width and height if undefined.
$width	= $imgwidth  if( !defined( $width ) );
$height = $imgheight if( !defined( $height ) );

# Resize if necessary
$image->Resize( geometry=> "${width}x${height}" )
    if( $width != $imgwidth || $height != $imgheight );

# Write output
die( "Error writing $outfile\n" )
    if( $image->Write( filename=>$outfile ) );
