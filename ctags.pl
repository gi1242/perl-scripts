#! /usr/bin/perl -w

use strict;

my $help=<<'***End Help***';
ctags version 1.0 by Gautam Iyer, Feb 2002.

USAGE:	ctags [-eiqrsvf] [-F <file>] [filelist]

DESCRIPTION:
	Check if your html tags match up.

OPTIONS:
    e	: Show all correctly matched end-tags
    f	: Tread all remaining arguements as files
    F	: Next arguement is file containing the list of files to check
    h	: Display this help and exit.
    i	: Show all ignored tags
    q	: Check if the quotes in tag attributes match up
    r	: Push incorrectly matched end tags back onto the stack
    s	: Show all unignored start tags encountered.
    v	: Equivalent to -ies
    x	: Next argument is a colon seperated list of tags to exclude

If the file list is ommited or is "-" then ctags reads from <STDIN>

***End Help***

my @stack;
my ($exptag, $etagline, $etagarg);
my ($tag, $line, $arg);
my $exceptions=":br:meta:hr:frame:img:input:";

my ($op_s, $op_e, $op_i, $op_q, $op_r) = (0,0,0,0,0);

for( ;$#ARGV >= 0 && substr($ARGV[0],0,1) eq "-"; shift) {
    if(index($ARGV[0],"e") >= 0)	{$op_e=1;}
    if(index($ARGV[0],"h") >= 0)	{print $help; exit;}
    if(index($ARGV[0],"i") >= 0)	{$op_i=1;}
    if(index($ARGV[0],"q") >= 0)	{$op_q=1;}
    if(index($ARGV[0],"r") >= 0)	{$op_r=1;}
    if(index($ARGV[0],"s") >= 0)	{$op_s=1;}
    if(index($ARGV[0],"v") >= 0)	{$op_i = $op_e = $op_s = 1;}
    if(index($ARGV[0],"x") >= 0) {
	if(defined($ARGV[1])) {$exceptions .= $ARGV[1] . ":"; shift;}
	else {die "Expecting exception list after -x"};
	next;
    }
    if(substr($ARGV[0],-1) eq "F") {
	if(!defined($ARGV[1]) or !open(FILE, "<$ARGV[1]"))
	   {die "Could not open filelist file"};

	while(<FILE>) {
	    chomp;
	    if($_) {$ARGV[$#ARGV+1] = $_};
	}
	close(FILE);
	shift;
	next;
    }
    if(substr($ARGV[0],-1) eq "f")	{last};
    if(substr($ARGV[0],1) =~ m/[^efFiqrsvx]/) {
	print "Unrecognized option: $ARGV[0]\n$help";
	exit;
    }
}

if($#ARGV == -1) {$ARGV[0] = '-'};

while($#ARGV >=0) {
    if(!open(FILE, "<$ARGV[0]")) {
	print "Could not open $ARGV[0]\n";
    } else {
	print "Processing $ARGV[0]:\n";
	for($line=1; <FILE>; $line++) {
	    while (m|<(/?\w+)(.*?)>|gc) {
		($tag, $arg) = ($1, $2);

		if (substr($tag,0,1) eq "/") {
		    ($etagarg, $etagline, $exptag) = (pop(@stack), pop(@stack), pop(@stack));

		    if(!defined($exptag)) {
			print "Line $line: Endtag <$tag> does not have a beginning\n";
		    } elsif(substr($tag,1) ne $exptag) {
			print "Line $line: Endtag <$tag> precedes endtag of <$exptag$etagarg> [line $etagline]\n";
			if($op_r) {push(@stack, $exptag, $etagline, $etagarg)};
		    } elsif($op_e) {
			print "Line $line: </$exptag> ends <$exptag$etagarg>[line $line]\n";
		    }
		} else {
		    if(index($exceptions,":\L$tag\E:") < 0) {
			if($op_q) {
			    if( $arg =~ m/^[^"]*("[^"]*"[^"]*)*\"[^"]*$/ )
				{print "Line $line: Unmatched \" in <$tag$arg>\n"};
			    if($arg =~ m/^[^']*('[^']*'[^']*)*\'[^']*$/)
			        {print "Line $line: Unmatched \' in <$tag$arg>\n"};
			}

			push(@stack, $tag, $line, $arg);
			if($op_s) {print "Line $line: Start-tag <$tag$arg>\n"};
		    } elsif($op_i) {
			print "Line $line: Ignored <$tag$arg>\n";
		    }
		}
	    }
	}
	close(FILE);

	while($#stack >= 0) {
	    ($etagarg, $etagline, $exptag) = (pop(@stack), pop(@stack), pop(@stack));
	    print "Unmatched <$exptag$etagarg>[line $etagline]\n";
	}
    }
} continue {
    shift;
    @stack=();
}
