#! /usr/bin/perl -w
# Created   : Fri 07 Sep 2007 09:22:18 PM GMT
# Modified  : Tue 18 Sep 2007 06:10:21 PM GMT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Fetch new files from digital camera

use strict;

use Cwd qw(realpath);
use File::stat;
use File::Glob qw(:globally :nocase);
use Getopt::Long qw(:config no_ignore_case bundling);
use Term::ANSIColor qw(:constants);
use Switch;

# Prototypes
sub bymtime($$);
sub get_mtimes;
sub print_usage;
sub ask_exec;
sub debug;
sub info;
sub warning;
sub error;
sub fatal;

# Options
my ($src_dir, $dst_dir);
my $ext	    = "*.jpg";
my $debug   = 0;
my $quiet   = 0;
my $pretend = 0;
my $ask	    = 0;
my $get	    = 0;
my $get_new = 0;
my $get_old = 0;
my $del_old = 0;
my $get_dup = 0;
my $put_dup = 0;
my $sync    = 0;

# Variables
my ($BD,$UL,$IT,$ER,$RE)=(RESET.CYAN,RESET.GREEN,RESET.YELLOW,RESET.RED,RESET);
my (@src_files, @dst_files);
my (%src_files_hash, %dst_files_hash);
my ($file, $i);
my @new_src_files = ();
my @old_src_files = ();
my @dup_src_files = ();

#
# Process options
#
GetOptions(
    "extension|x=s"	=> \$ext,
    "quiet|q"		=> \$quiet,
    "pretend|p"		=> \$pretend,
    "ask|a"		=> \$ask,
    "get|g"		=> \$get,
    "get-new"		=> \$get_new,
    "get-old"		=> \$get_old,
    "del-old"		=> \$del_old,
    "get-dup"		=> \$get_dup,
    "put-dup"		=> \$put_dup,
    "sync|s"		=> \$sync,

    "debug|D=i"		=> \$debug
) or print_usage();

$src_dir = realpath( shift( @ARGV ) );
$dst_dir = realpath( shift( @ARGV ) );

print_usage() unless( defined( $src_dir ) && defined( $dst_dir ) );

if( $get )
{
    $get_old = 1;
    $get_new = 1;
}

if( $sync )
{
    $del_old = 1;
    $get_new = 1;
}


chdir( $dst_dir )
    or fatal( "Could not change to directory $dst_dir" );
@dst_files = sort bymtime get_mtimes( glob( $ext ) ) ;

chdir( $src_dir )
    or fatal( "Could not change to directory $src_dir" );
@src_files = sort bymtime get_mtimes( glob( $ext ) );

if( $debug >= 9 )
{
    debug( 9, "Src files ($src_dir):" );
    for $file (@src_files)
    {
	print STDERR "$file->{name} $file->{mtime}\n"
    }

    debug( 9, "Dst files ($dst_dir):" );
    for $file (@dst_files)
    {
	print STDERR "$file->{name} $file->{mtime}\n"
    }
}

# Create a hash of src and dst files (for easy lookup by name).
for $file (@dst_files)
{
    $dst_files_hash{ $file->{name} } = $file->{mtime};
}

for $file (@src_files)
{
    $src_files_hash{$file->{name}} = $file->{mtime};
}

debug( 2, "Source files: ", join( ", ", sort( keys( %src_files_hash ) ) ) );
debug( 2, "Dest files: ", join( ", ", sort( keys( %dst_files_hash ) ) ) );
debug( 1, $#dst_files + 1, " source files." );
debug( 1, $#src_files + 1, " dst files." );

if( !defined( $dst_files[0] ) )
{
    info( "Dest dir $dst_dir empty." );
    @new_src_files = keys( %src_files_hash );
}

else
{
    foreach $file (@src_files)
    {
	if( $file->{mtime} <= $dst_files[0]->{mtime} )
	{
	    # File is old
	    push( @old_src_files, $file->{name} )
		if( !exists( $dst_files_hash{$file->{name}} ) )
	}
	else
	{
	    # Source file is new.
	    if( exists( $dst_files_hash{ $file->{name} } ) )
	    {
		debug( 9, "Source file $file->{name} is new (duplicate)." );
		push( @dup_src_files, $file->{name} )
	    }

	    else
	    {
		debug( 9, "Source file $file->{name} is new." );
		push( @new_src_files, $file->{name} )
	    }
	}

    }
}

# Need to be in src_dir before going any further. (We're already in src_dir, so
# need to do nothing now).
if( @old_src_files )
{
    if( $get_old && $del_old )
    {
	fatal( "Can't both get and delete old files" )
    }
    elsif( $get_old )
    {
	ask_exec( 'cp', '--preserve=timestamps', '-v',
	    @old_src_files, $dst_dir );
    }
    elsif( $del_old )
    {
	ask_exec( 'rm', '-vf', @old_src_files );
    }
    else
    {
	warning( scalar(@old_src_files),
	    " old source files missing in destination:\n",
	    join( " ", @old_src_files ) )
    }
}

if( @dup_src_files )
{
    if( $get_dup && $put_dup )
    {
	fatal( "Can't both get and put duplicate files" )
    }
    elsif( $get_dup )
    {
	ask_exec( 'cp', '--preserve=timestamps', '-fv',
	    @dup_src_files, $dst_dir );
    }
    elsif( $put_dup )
    {
	chdir( $dst_dir ) or fatal( "Could not chdir($dst_dir)" );
	ask_exec( 'cp', '--preserve=timestamps', '-fv',
	    @dup_src_files, $src_dir );
	chdir( $src_dir ) or fatal( "Could not chdir($src_dir)" );;
    }
    else
    {
	warning( scalar( @dup_src_files ),
	    " new source files are duplicate in destination:\n",
	    join( " ", @dup_src_files ) )
    }
}

if( $get_new )
{
    if( @new_src_files )
    {
	ask_exec( "cp", '--preserve=timestamps', '-v',
	    @new_src_files, $dst_dir );
    }
    else
    {
	info( "No new source files" );
    }
}


#{{{1 functions
sub bymtime($$)
{
    $_[1]->{mtime} <=> $_[0]->{mtime};
}

sub get_mtimes
{
    my $file;
    my ( $latest_modtime, $most_recent_file );


    foreach $file (@_)
    {
	switch( ref( $file ) )
	{
	    case "HASH"
	    {
		1;
	    }

	    case ""
	    {
		my $stat = stat( $file ) or error( "Could not stat $file" );

		$file = { mtime => $stat->mtime, name => $file };
	    }

	    else
	    {
		fatal( "$file not a SCALAR or HASH" );
	    }
	}

    }

    return @_;
}


sub print_usage
{
    print <<"EOF";
USAGE
    dirsync [options] <src-dir> <dst-dir>

OPTIONS
    -x		Glob pattern of files to consider (default $ext).
    -q		Suppress info and warning messages.
    -p		Pretend. Only show what would be done.
    -a		Ask before doing anything.
    -g		Get new and old files (ignore duplicates).
    -s		Get new files, and delete old (missing) files.
    -D N	Set debug level to N (default $debug).
EOF
    exit(1);
}

sub debug
{
    my $level = shift;

    print STDERR "${BD}Debug:${RE} ", @_,  "\n"
	if( $level <= $debug );
}

sub ask_exec
{
    my @args = @_;
    my $exec = 0;

    if( $pretend || $ask )
    {
	print( "${BD}Exec: ${RE}@args" );
	if( $ask )
	{
	    my $line;

	    print( "${BD}? (y/N)$RE" );
	    $line = <STDIN>;
	    chomp( $line );

	    $exec = 1 if( $line =~ m/^y|yes$/i );
	}
	else
	{
	    print( "\n" );
	}
    }
    else
    {
	$exec = 1;
    }

    if( $exec )
    {
	#print( "Would exec @args\n" );
	system( @args ) == 0
	    or fatal( "Could not exec '@args'" );
    }
}

sub info
{
    print STDERR "${UL}", @_,  "${RE}\n"
	if( !$quiet);
}

sub warning
{
    print STDERR "${IT}Warning:${RE} ", @_,  "\n"
	if( !$quiet);
}

sub error
{
    print STDERR "${ER}Error:${RE} ", @_,  "\n"
}

sub fatal
{
    print STDERR "${ER}ERROR:$RE ";
    die( @_, "\n" );
}
