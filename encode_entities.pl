#! /usr/bin/perl -w
# Created   : Mon 17 Jan 2011 11:02:11 PM EST
# Modified  : Mon 17 Jan 2011 11:02:11 PM EST
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'

use strict;
use HTML::Entities;

while(<>)
{
    print( encode_entities( $_ ) );
}
