#! /usr/bin/perl -w
# epoch2date [+FORMAT] TIME
# Takes the no of seconds since epoch and prints the date and time

use strict;
use POSIX qw(strftime);

my ($time, $format) = (time(), "%c%n");

while( scalar(@ARGV)) {
    if( substr( $ARGV[0], 0, 1) eq "+" ) {
	$format = substr( $ARGV[0], 1);
    } else {
	$time = $ARGV[0];
    }
    shift;
}

print strftime( $format, localtime( $time));
