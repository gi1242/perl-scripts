#! /usr/bin/perl -w

use strict;

my $line;

while( $line = <>) {
    if($line =~ m/^.*?(?=\.)/) {
	print "$&\n";
    } else {
	print $line;
    }
}
