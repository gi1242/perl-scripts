#! /usr/bin/perl -w
# Print the gateway/router address.

my $defroute = `/sbin/ip route show to match 171/8`;
if( $defroute =~ m/^default via ((?:\d{1,3}\.){3}\d{1,3})(?:\D|$)/m )
{
    print( "$1\n" );
    exit(0);
}
else
{
    exit(1);
}
