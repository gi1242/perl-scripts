#! /usr/bin/perl -w
# Created   : Thu 26 May 2016 10:32:13 PM EDT
# Modified  : Wed 12 Oct 2016 12:20:46 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'

use strict;
use Getopt::Std;
use IPC::Run qw(run);
use IPC::System::Simple qw(capturex systemx);
use List::Util qw(first);

my %opt = ();

# Process options: a: all, v: verbose, h: help, A: Attr list, i: ignore attrs
getopts( 'advhiA:', \%opt ) or exit(1);
#print( join( ' ', keys( %opt ) ) );

exec( qw(perldoc -tT), $0 )
    if( defined( $opt{h} ) );

my $debug = defined( $opt{d} );

my @attrs;
@attrs = defined( $opt{A} ) ? split( ',', $opt{A} ) :
    qw(text eol crlf ident filter);


chomp( my $dir = qx(git rev-parse --show-toplevel) );
chdir( $dir ) or die( "Could not cd $dir\n" );

if( defined( $opt{a} ) )
{
    # Stash changes and re-checkout all files
    my $stashed = 0;

    if( system( qw(git diff --quiet)) )
    {
	system( qw(git stash), defined( $opt{v} ) ? qw(save) : qw(save -q) )
	    and die( "Unable to stash\n" );
	$stashed = 1;
    }

    print( "Re-checking out all files\n" ) if( defined( $opt{v} ) );
    system( qw( git checkout @ --), $dir );

    if( $stashed )
    {
	system( qw(git stash), defined( $opt{v} ) ? qw(pop) : qw(pop -q) )
	    and die( "Couldn't pop stash\n" );
    }
}
else
{
    my (@changed_files, @commit_files, $file_attrs );

    @changed_files = qx(git diff --name-only );
    die( "Error running git diff\n" ) if( $? );
    chomp( @changed_files );
    @commit_files =
	qx(git diff-tree --no-commit-id --name-only -r --diff-filter=d \@);
    die( "Error running git diff-tree\n" ) if( $? );
    chomp( @commit_files );
    if( !defined( $opt{i} ) )
    {
	run( [qw(git check-attr --stdin), @attrs],
	    \join( "\n", @commit_files ), \$file_attrs )
	    or die( "Couldn't get attributes\n" );
    }

    print( "Changed: ", join( ' ', @changed_files ), "\n" ) if( $debug );
    print( "Commit: ", join( ' ', @commit_files ), "\n" ) if( $debug );
    print( "attrs: \n", $file_attrs, "\n" )
	if( defined($file_attrs) && $debug );

    foreach my $fname (@commit_files) {
	if( !defined( $opt{i} ) &&
	    $file_attrs !~ m/^$fname: [a-z]+: (?!(?:unset|unspecified)$)/im )
	{
	    print( "Skipping $fname (no attrs match)\n" )
		if( defined( $opt{v} ) );
	}
	elsif ( contains( $fname,  @changed_files ) )
	{
	    print( "Skipping $fname (local modifications).\n" )
		if( defined( $opt{v} ) );
	}
	else
	{
	    print( "Re-checking out $fname\n" ) if( defined( $opt{v} ) );
	    # capturex Dies on error!
	    capturex( qw(git rm --cached --), $fname );
	    system( qw(git checkout @ --), $fname )
		and print( "Error checking out $fname\n" );
	}
    }
}

sub contains
{
    my $e = shift();
    foreach my $i (@_)
    {
	return 1 if( $e eq $i );
    }
    return 0;
}

# {{{1 POD documentation
=head1 NAME
git-refilter.pl - Re-run git's filters on all (unmodified) files in HEAD

=head1 DESCRIPTION

This re-run git's filters by re-checking out a set of files. This will, for
instance, update the C<Id> tag (if you have the C<ident> git attriute enabled).
By default, only unmodified files that were changed in HEAD and have a git
checkin attribute set are re-checked out. This is safe to run as a post-commit
hook to keep C<Id> up to date, for instance.

=head1 OPTIONS

=over

=item B<-a>

Stash modifications and re-checkout all files, not just the ones modified in
HEAD.

=item B<-i>

Ignore attributes. Re-checkout all files touched by HEAD that don't have local
modifications.

=item B<-A> I<attr1,attr2,...>

Comma separated list of attributes. At least one must be set for file to be
re-checkedout

=item B<-v>

Be more verbose

=back
