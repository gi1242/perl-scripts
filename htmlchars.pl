#! /usr/bin/perl -w

use strict;

undef $/;

my ($tag, $cdata, $comment);
$_=<STDIN>;

print "<html><body><table align=center>\n";
print "<tr valign=top><td><b>Symbol</b></td><td><b>Name</b></td><td><b>Code</b></td><td><b>Description</b></td></tr>\n";

    while(m/<!ENTITY\s+(\w+)\s+CDATA\s+\"&\#(\d+);\"\s+--\s+(.*?)\s+-->/gs) {
	($tag, $cdata, $comment) = ($1,$2,$3);
	$comment =~ s/\s*\n\s*/<br>/s;

	print "<tr valign=top><td align=center>&$tag;</td><td>$tag</td><td>$cdata</td><td>$comment</td></tr>\n";
    }
print "</table></body></html>\n";
