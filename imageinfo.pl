#! /usr/bin/perl -w

use strict;
use Image::Magick;
use Image::ExifTool qw(:Public);

my $file = "/home/gautam/.thumbnails/normal/gqview-8848c59445041f53fde6896334c11bc2.png";

=pod
my $magick = Image::Magick->new();
$magick->Read( $file );
#print( "Set( 'Thumb::URI', 'hello' ): ",
#   $magick->Set( "text", "hello" ), "\n" );
print( $magick->Get( "Thumb::URI" ), "\n" );

=cut

my $exiftool = Image::ExifTool->new();
my %options;
$exiftool->ExtractInfo( $file, \%options );
print( "Group: ", $exiftool->GetGroup( "Thumb::URI" ), "\n" );
$exiftool->SetNewValue( ":URI", "file:///free/pics/2007/egypt/dsc03051.jpg",
    Group => 'PNG' );
print( join( "\n", $exiftool->GetFoundTags( 'File' ) ), "\n" );
