#! /usr/bin/perl -w
# Created   : Wed 27 Jun 2012 03:52:09 PM EDT
# Modified  : Sat 10 Nov 2012 05:54:35 PM EST
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'
#
# Sort an AMSRefs ltb database

use strict;
use Term::ANSIColor qw(:constants);

sub debug;
my ( $dbgError, $dbgWarning, $dbgInfo, $dbgMoreInfo ) = (0, 1, 2, 3 );
my $dbgLevel = $dbgWarning;

my ($BD,$UL,$IT,$ER,$RE)=(RESET.CYAN,RESET.GREEN,RESET.YELLOW,RESET.RED,RESET);
my $debug = 1;

my $line;
my $header = 1;
my $footer = 0;
my @refs = ();

if( $#ARGV >= 0 )
{
    if($ARGV[0] eq "-v" )
    {
	$dbgLevel = $dbgInfo;
    }
    else
    {
	# Print usage
	print STDERR <<"EOF";
Sorts an ltb database on stdin, and writes the result to stdout. It generates
sort keys by using the author names with accents stripped and the year. You can
override the generated sort key by including a comment line of the form

    \% sortkey = YOUR SORT KEY

Use -v to see the generated sort keys. For this script to work your file must
adhere to the following rules

    1. The "\\bib{..." must appear in the first column
    2. The matching "}" must appear in the first column
    3. The date / year field should contain a 4 digit year if you want it
       added to the sort key.
    4. All comments between \\begin{biblist} and \\end{biblist} will be
       deleted. The rest of your file is untouched.

Send feedback to Gautam Iyer (Google me). Your comments (other than patches, or
words of praise) will be ignored.
EOF
	exit(1);
    }
}

while( $line = <STDIN> )
{
    #chomp($line);

    print $line if( $header || $footer );
    next if( $footer );

    $header = 0
    if( $line =~ m/^\\begin\{biblist\}/ );

    if( $line =~ m/^\\bib\{/ )
    {
	my %bibentry;
	my $refline = $line;
	my $year = undef;
	my $sortkey = undef;

	$bibentry{text} = $refline;
	$bibentry{sortkey} = '';
	REFLINE: while( $refline = <STDIN> )
	{
	    $bibentry{text} .= $refline;

	    last REFLINE
		if( $refline =~ m/^\}/ );

	    next REFLINE
		if( defined( $sortkey ));

	    if( $refline =~ m/^\s*\%\s*sortkey\s*=\s*(.*?)\s*$/ )
	    {
		$sortkey = $1;
	    }

	    elsif ( $refline =~ m/^\s*author\s*=\s*\{\s*(.*?)\s*\}\s*,\s*$/i )
	    {
		my $author = $1;

		if( $author !~ m/,/ )
		{
		    # Assume authors is Fname MI Lname
		    $author =~ s/^(.*)\s([^[:space:]]+)$/$2, $1/;
		}


		$author =~ s/\s*,\s*/ /g;	# Lexical order: "A, B" is
						# before "Aa, B"

		$author =~ s/(?:\\\W|[{}])//g;	# Get rid of accents
		$author =~ s/\\(\w)/$1/g;	# Replace \O, \i etc with O & i.
		$author =~ s/\./ /g;		# Get rid of .

		$author =~ s/\s{2,}/ /g;	# Get rid of multiple spaces
		$author =~ s/\s$//;	    	# Get rid of trailing spaces

		$bibentry{sortkey} .= uc($author) . ' ';
	    }

	    elsif( $refline =~
		m/^\s*(?:date|year)\s*=\s*\{.*\b([0-9]{4})\b.*\}\s*,\s*$/i )
	    {
		$year = $1;
	    }
	}

	if( defined( $sortkey ) )
	{
	    $bibentry{sortkey} = $sortkey;
	}

	else
	{
	    debug( $dbgError, "No authors for sort key in bib item ",
		substr( $bibentry{text}, 5, 20 ) =~ s/\n//rg )
		if( $bibentry{sortkey} eq '' );

	    if( !defined( $year ) )
	    {
		debug( $dbgWarning,
		    "No date (or date is not a 4 digit year)in item ",
		    substr( $bibentry{text}, 0, 20 ) =~ s/\n//rg );
	    }
	    else
	    {
		# Append the year after all authors for the sort key.
		$bibentry{sortkey} .= $year;
	    }
	} # defined( $sortkey )

	debug( $dbgWarning,
	    "Non word characters in sort key $bibentry{sortkey}" )
	    if( $bibentry{sortkey} =~ m/[^A-Z0-9 -]/ );
	debug( $dbgInfo, "Sort key '$bibentry{sortkey}'" );

	$refs[$#refs + 1] = \%bibentry;
    }

    elsif( $line =~ m/^\\end\{biblist\}/ )
    {
	$footer = 1;

	# TODO sort refs
	@refs = sort { $a->{sortkey} cmp $b->{sortkey} } @refs;

	# Print refs
	for my $bib (@refs)
	{
	    print $bib->{text};
	}

	print $line;
    }
}

STDERR->print( "Processed ${UL}", $#refs + 1, "$RE records.\n" );

sub debug
{
    my $level = shift;
    my @header = ( "${ER}Error:$RE", "${IT}Warning:$RE", "${UL}Info:$RE", "Info:" );

    STDERR->print( $header[$level], ' ', @_,  "\n" )
	if( $level <= $dbgLevel );
}
