#! /usr/bin/perl -w
# Created   : Mon 19 Jan 2009 02:26:35 PM PST
# Modified  : Thu 11 Dec 2014 12:04:12 AM CST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#	      Modified from http://www.vinc17.org/mutt/mutt-mailto

use strict;
use URI;
use File::Temp;

my $proc = "mailto-mutt";

$ARGV[0] =~ /^mailto:/ or die "Usage: $proc <mailtoURI>\n";

my $u = URI->new($ARGV[0]);
my %headers = $u->headers;
my $to = $u->to;
my @cmd = -t STDIN ? qw/mutt/ : (qw/xterm -e mutt/);
my $body;
while (my ($k,$v) = each(%headers))
{
    if( lc($k) eq 'bcc' )
	{ push( @cmd, '-b', $v ) }
    elsif( lc($k) eq 'cc' ) 
	{ push( @cmd, '-c', $v ) }
    elsif( lc($k) eq 'subject' )
	{ push( @cmd, '-s', $v ) }
    elsif( lc($k) eq 'body' )
	{ $body = $v; }
}

if (defined $body)
{
    my $tmp = new File::Temp();
    die( "Could not open temporary file\n$!\n" )
    if( !defined( $tmp ) );
    print $tmp "$body\n"
	or die("$proc: can't print to temporary file\n$!\n");
    close($tmp)
	or die "$proc: can't close temporary file\n$!\n";
    system( @cmd, '-i', $tmp, $to);
}
else
{
    system( @cmd, $to );
}

# Test on:
# mailto:a1@x?To=a2@x&Cc=a3@x&Bcc=a4@x&Subject=mailto%20test
# mailto:a1@x?To=a2@x&Cc=a3@x&Bcc=a4@x&Subject=mailto%20test&body=The%20body.
