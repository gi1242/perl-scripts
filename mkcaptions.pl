#! /usr/bin/perl -w

use strict;

my $help= <<'.';
mkslides 1.0 created by Gautam Iyer April 2002

Description:
    Creates html slides from jpeg files.

Input (stdin):
    
    <imgname>
    [Title:	Title] (Empty line signifies no title)
    [SlideName:	<htmlslidename>]
    [PNButtons:	T/F] (Defaults to T)
    [FLButtons:	T/F] (Defaults to F)
    [Caption:	Caption]
    [LCaption:
    Multiline caption ending in a
    line containing . alone]
    
    [Footnote:
    Multiline footnote,
    ending in a line containing . alone]
    <blank line ending slide>
    
    <image2>
    ...
    
    The options Title, [PN|FN]Buttons, Footnote are persistant.
    
Output:
    Html slides with the same name as each image.
.



my ($line, $option, $value);
my (@slidenamen, @filenamen, @captionn, @titlen, @pnbuttonsn, @flbuttonsn, @ibuttonn);
my ($title, $pnbuttons, $flbuttons, $ibutton) = ("", 't', 'f', 't');
my ($first, $last, $previous, $next);
my ($nslides, $i, $j);

for($nslides=0; $line=<STDIN>; $nslides++) {
    if(substr($line,0,1) eq '#' || $line =~ m/^\s*\n/) {next;};

    ($filenamen[$nslides] = $line) =~ m/(.*)\./;
    chomp $filenamen[$nslides];
    $slidenamen[$nslides] = "$1.html";

    for( $captionn[$nslides] = "";
	 ($line=<STDIN>) && !($line =~ m/^\s*\n/);
       ) {

	unless($line =~ m/^(\w*)\s*:\s*(.*)$/)
	    {die "Bad syntax at $line"};

	$option = "\L$1\E";
        $value = $2;

      SWITCH: {
	  if($option eq "title")     {$title = $value};
	  if($option eq "pnbuttons") {$pnbuttons = "\L$value\E"};
	  if($option eq "flbuttons") {$flbuttons = "\L$value\E"};
	  if($option eq "slidename") {$slidenamen[$nslides] = $value};
	  if($option eq "caption")   {$captionn[$nslides] = $value};
	  if($option eq "ibutton")   {$ibutton = "\L$value\E"};
      }
	$titlen[$nslides] = $title;
	$pnbuttonsn[$nslides] = $pnbuttons;
	$flbuttonsn[$nslides] = $flbuttons;
	$ibuttonn[$nslides] = $ibutton;
    }

    print("Slide	: $slidenamen[$nslides]\n",
          "Title	: $titlen[$nslides]\n",
          "Image	: $filenamen[$nslides]\n",
	  "Caption	: $captionn[$nslides]\n",
	  "PNButtons: $pnbuttonsn[$nslides]\n",
          "FLbuttons: $flbuttonsn[$nslides]\n",
	  "IButton  : $ibuttonn[$nslides]\n\n");
}

for($i=0; $i<$nslides; $i++) {
    open(SLIDE,">$slidenamen[$i]") or die "Unable to open $slidenamen[$i]";

    print SLIDE <<".";
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta name="Author" content="Gautam Iyer">
</head>
<body text=black bgcolor=white link=blue vlink=darkblue alink=darkred>
<table align=center>
.

    if(defined $titlen[$i]) {
	print( SLIDE
	       "<tr>\n<td></td>\n",
	       "<td>$titlen[$i]</td>\n<td></td>\n</tr>\n" )
	};


    $first = ($flbuttonsn[$i] eq 't' ? "<a href=\"$slidenamen[0]\">First</a> " : "");
    $last = ($flbuttonsn[$i] eq 't' ? " <a href=\"$slidenamen[$nslides-1]\">Last</a>": " ");

    if($pnbuttonsn[$i] eq 't') {
	$previous = ($i == 0 ? "Previous" : "<a href=\"$slidenamen[$i-1]\">Previous</a>");
	$next = ($i == $nslides-1 ? "Next" : "<a href=\"$slidenamen[$i+1]\">Next</a>");
	print( SLIDE
	       "<tr>\n<td align=left><font size=-1>&nbsp;<br>",
	       $first, $previous,
	       "<br>&nbsp;<br></font></td>\n"
	     );

	print SLIDE
	    $ibuttonn[$i] eq 't' ? "<td align = center><font size=-1>&nbsp;<br><a href=\"index.html\">Index</a><br>&nbsp;<br></font></td>\n" : "<td></td>\n";

	print( SLIDE
	       "<td align=right><font size=-1>&nbsp;<br>",
	       $next, $last,
	       "<br>&nbsp;<br></font></td>\n",
	       "<tr>\n"
	     );
    };

    print(SLIDE "<tr>\n<td colspan=3 align=center><img src=\"$filenamen[$i]\"></td>\n</tr>\n");

    if(defined $captionn[$i]) {
	print(SLIDE "<tr>\n<td colspan=3 align=center>&nbsp;<br>$captionn[$i]</td>\n</tr>\n")
    };

    print SLIDE "</table>\n</body>\n</html>\n";
    close SLIDE;
};

open(INDEX,">index.html") or die "Unable to open index.html";

print INDEX <<".";
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <title>Page Title</title>
</head>
<body text=black bgcolor=white link=blue vlink=darkblue alink=darkred>
<table border=0 cellspacing=2 cols=3 width=615 align=center>
<tr align=center>
<td colspan=3><h1>Page Title</h1></td>
</tr>
.

for($i=0; $i<$nslides; $i+=3) {
    print INDEX "<tr align=center>\n";
    for($j=0; $j<3; $j++) {
	print INDEX "<td><a href=\"$slidenamen[$i+$j]\"><img src=\"thumbnails/$filenamen[$i+$j]\"></a></td>\n"
	    if($i+$j<$nslides);
    }

    print INDEX "</tr>\n<tr align=center>\n";

    for($j=0; $j<3; $j++) {
	print INDEX "<td>$captionn[$i+$j]</td>\n"
	    if($i+$j<$nslides);
    }
}

print INDEX <<'.';
<tr><td colspan=3 align=left>
&nbsp;<br>
&nbsp;<br>
<font size=-1>
Questions? Comments? Send me hate mail? <a href="mailto:gautam@math.uchicago.edu">gautam@math.uchicago.edu</a>. Public Key <a href="http://zaphod.uchicago.edu/~gautam/public_key.txt">03C4B6C8</a>.<br>
<hr><br>
<i></i>
<!-- Created: Thu Jun 14 12:28:00 CST 2001 -->
<!-- hhmts start -->
Last modified: Fri Jul 27 14:41:30 CDT 2001
<!-- hhmts end -->
</font>
</td></tr></table>
</body>
</html>
.

close INDEX;
