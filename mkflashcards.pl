#! /usr/bin/perl -w
# Created   : Sat 26 Nov 2011 02:03:14 PM EST
# Modified  : Sat 26 Nov 2011 03:30:21 PM EST
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'
#
# Take a PDF of flash cards, and randomize it.

use strict;

my $nCards=shift;
my $input=shift;
my $output=shift;

if( !defined( $nCards ) || !defined( $input ) || !defined( $output ) )
{
    print( "Usage: mkflashcards #cards input.pdf output.pdf\n" );
    exit(1);
}

my @slides;

for my $i (0 .. ($nCards-1))
{
    $slides[$i] = $i;
}

my @neworder=();

for my $i (0 .. ($nCards-1))
{
    my $j= int( rand( $#slides + 1) );
    $neworder[$#neworder+1] = splice( @slides, $j, 1 );
}

my $command = "pdftk $input cat ";
for my $i (@neworder)
{
    $command = $command . (2*$i+1) . ' ' .  (2*$i +2) . ' '
}
$command = substr( $command, 0, -1 ) . " output $output";

print "$command\n"
