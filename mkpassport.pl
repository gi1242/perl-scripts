#! /usr/bin/perl -w
# Make a passport collage

use Image::Magick;

my ($xsize, $ysize, $xoff, $yoff) = (256*3 + 18, 256*2 + 12, 9, 6);
my $image = Image::Magick->new();
