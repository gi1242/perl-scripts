#! /usr/bin/perl -w
# Create of all files in a given directory.

use strict;
use Switch;
use Image::Magick;
use Image::ExifTool;
use Cwd qw(realpath);
use Getopt::Long qw(:config no_ignore_case bundling);
use Digest::MD5 qw(md5_hex);
use File::Path;

# Options
my $exif_tag = 'PreviewImage';
my @dirs     = ('.');
my $tmb_size = '128x128';
my $tmb_dir  = "$ENV{HOME}/.thumbnails/normal/";
my $exec_prog;

# Variables
my $dir;
my $exifinfo = Image::ExifTool->new();
my $image = Image::Magick->new();

# Prototypes
sub print_help;
sub make_thumbnail;

GetOptions(
    "exif-tag|t=s"	    => \$exif_tag,
    "thumbnail-size|s=s"    => \$tmb_size,
    "thumbnail-dir|d=s"	    => \$tmb_dir,
    "exec|e=s"		    => \$exec_prog
) or print_help();

@dirs = @ARGV if( @ARGV );

mkpath( $tmb_dir ) if( ! -d $tmb_dir );

foreach $dir (@dirs)
{
    my ($nthumbs, $nerrors, $nskipped) = (0, 0, 0);
    $dir = realpath( $dir );
    
    for my $file (glob( "$dir/*.jpg" ))
    {
	switch( make_thumbnail( $file ) )
	{
	    case 1	{ $nthumbs++ }
	    case 0	{ $nerrors++ }
	    case -1	{ $nskipped++ }
	}

	print( "Dir $dir: $nthumbs thumbnails, $nerrors errors, ",
		"$nskipped skipped.\n\e[F" )
	    if( -t STDOUT );
    }
    print( "\n" );
}

# {{{1 Functions
sub make_thumbnail
{
    my $file = shift;

    my ($orientation, $origHeight, $origWidth, $thumbnail );
    my ($width, $height);
    my $success = 0;

    my $tfile = $tmb_dir . md5_hex( "file://" . realpath($file) ) . ".png";

    # Check if a thumbnail exists and is newer
    return -1
	if( -e $tfile && ( -M $file <= -M $tfile ) );

    $exifinfo->ExtractInfo( $file, {Binary=>1} );

    $orientation = $exifinfo->GetValue( 'Orientation', 'ValueConv' );
    $origHeight  = $exifinfo->GetValue( 'ImageHeight', 'ValueConv' );
    $origWidth   = $exifinfo->GetValue( 'ImageWidth' , 'ValueConv' );
    $thumbnail   = $exifinfo->GetValue( $exif_tag, 'Raw' );

    return 0
	unless( defined( $orientation ) && defined( $origHeight )
		&& defined( $origWidth ) );

    return 0
	unless( defined( $thumbnail ) );

    return 0
	if( $image->BlobToImage( $$thumbnail ) );

    ($width, $height) = $image->Get( 'columns', 'rows' );
    return 0
	unless( defined( $width ) && defined( $height ) );

    # Check aspect ratios and crop
    if( int( 0.5 + $origHeight / $origWidth * $width ) < $height )
    {
	# Black horizontal border
	my $newheight = int( 0.5 + $origHeight / $origWidth * $width );
	my $y = ($height - $newheight) / 2;

	$image->Crop( geometry=>"${width}x$newheight+0+$y" );
	#print STDERR "${width}x$newheight+0+$y:";
    }
    elsif( int( 0.5 + $origWidth / $origHeight * $height ) < $width )
    {
	# Black vertical border
	my $newwidth = int( 0.5 + $origWidth / $origHeight * $height );
	my $x = ($width - $newwidth) / 2;

	$image->Crop( geometry=>"${newwidth}x$height+$x+0" );
	#print STDERR "${newwidth}x$height+$x+0";
    }

    # Resize / rotate the image.
    resize_rotate( $image, $orientation, $tmb_size );

    $image->Set( filename=>$file );
    $image->Thumbnail( $tmb_size );
    $image->Write( filename=>$tfile, depth=>8 );

    # Clear
    @$image=();

    return 1;
}

sub resize_rotate
{
    my ($image, $orientation, $size) = @_;
    my $retval = 0;

    my ($width, $height) = $image->Get( 'columns', 'rows' );

    my ($new_width, $new_height) = split( 'x', $size );

    return 0
	unless( defined( $new_width ) && defined( $new_height ) );

    # Resize if necessary
    if(
	 $width > $new_width ||
	 $height > $new_width
      )
    {
	#print STDERR "r";
	$image->Resize(
		geometry => $size,
		filter	 => 'Cubic',
		#blur	 => .75
	    );
    }

    # Rotate thumbnail based on orientation.
    switch( $orientation )
    {
	case /^(right_top|6)$/	{ $image->Rotate( degrees=>90) }
	case /^(left_bot|8)$/	{ $image->Rotate( degrees=>-90) }
    }

    return 1;
}

sub print_help
{
    print( <<"EOF" );
USAGE:
    mkthumbnails [options] [dirs...]

Makes thumbnails of all images in the specified directories, and stores them in a format understandable by file managers / Gnome / KDE based image viewers. Some image viewers don't use the preview image in the exif header, so take a very long time to generate thumbnails. This program can be run to quickly generate thumbnails before you launch a GUI based program to view files in that directory.

OPTIONS:
    -e tag  : Exif tag to use for preview image ($exif_tag)
    -s size : Thumbnail size ($tmb_size)
    -d dir  : Directory to store thumbnails in ($tmb_dir)
    -e cmd  : Command to execute after thumbnails are created.
EOF
    exit(1);
}
