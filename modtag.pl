#! /usr/bin/perl -w
# Created   : Tue 04 Nov 2008 04:19:52 PM PST
# Modified  : Tue 04 Nov 2008 05:51:05 PM PST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Modifies mail tags

use strict;
use Getopt::Long qw(:config no_ignore_case bundling);
use Term::ANSIColor qw(:constants);
use File::Temp;

my ($BD,$UL,$IT,$ER,$RE)=(RESET.CYAN,RESET.GREEN,RESET.YELLOW,RESET.RED,RESET);

my ($msg, @tags, $tagname);

GetOptions(
    "add-tag|a=s"	=> \$tagname
) or die;

$msg=shift;

if ( defined($tagname) )
{
    my $line = `formail -cx X-Label < $msg`;
    my $exists=0;

    $line =~ s/^\s*(.*?)\s*$/$1/g;
    @tags=split( /\s*,\s*/, $line);

    foreach my $i (@tags)
    {
	if( $i eq $tagname )
	{
	    $exists = 1;
	    last
	}
    }

    if( $exists == 0 )
    {
	my  $tmpfile  = mktemp( "modtag.XXXXXX" );
	system( "formail -I 'X-Label: " . join( ", ", @tags, $tagname ) .
	    "' < $msg > $tmpfile");
	system( "mv -f $tmpfile $msg" );
    }

    #print( "tagname=$tagname, tags=", join( ':', @tags), ", msgfile=$msg\n");
}
