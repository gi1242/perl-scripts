#! /usr/bin/perl -w
# Print sender and subjects in all new messages for a maildir style 

use strict;

use Term::ANSIColor qw(:constants);
my ($RED, $RESET) = (RED, RESET);

my $maildir = "/var/spool/mail/$ENV{USER}";
my ($filename, $sender, $email, $subject);

my $debug;
# $debug = 1;

foreach $filename ( glob( "$maildir/new/*")) {
    ($sender, $email, $subject) = ("(unknown)", undef, "(unknown)");

    # Open the email message
    unless( open( MSGFILE, "<$filename")) {
	print STDERR "${RED}Unable to open $filename$RESET\n";
	next;
    }

    # Grep the header for sender and subject
    while( <MSGFILE>) {
	chomp;
	last if( $_ eq "");
	print STDERR CYAN, $_, RESET, "\n" if( defined( $debug));

	m/^From: +(.+?)(?:\s+(<.*>))?/ && do {
	    ($sender, $email) = ($1, $2);
	    next;
	};

	m/^From: +((?:\w|[=.+-])+@(?:\w|[.-])+[[:alpha:]]{2})/ && do {
	    ($sender, $email) = ( undef, $1);
	    next;
	};

	m/^Subject: +(.*)$/ && do {
	    $subject = $1;
	    next;
	};
    }

    close( MSGFILE);
    if( defined( $email)) {
	printf STDOUT "%-20.19s".YELLOW."%.60s$RESET\n", $sender, $subject;
    } else {
	printf STDOUT "%-20.19s".YELLOW."%.60s$RESET\n", $sender, $subject;
    }
}
