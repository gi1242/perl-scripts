#! /usr/bin/perl -w
# Created   : Thu 13 Dec 2007 01:08:07 PM PST
# Modified  : Thu 13 Dec 2007 01:37:31 PM PST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Convert a password file to mutt aliases.

use strict;
use Getopt::Long qw(:config no_ignore_case bundling);

my $line;

# Options
my $uid_min = 1000;
my $domain;

GetOptions(
    "uid-min|u=n"	=> \$uid_min,
    "domain|d=s"	=> \$domain
) or die( "Usage: ypcat passwd | passwd_to_alias.pl [-u uid_min] -d domain\n" );

die( "Error: No domain specified\n" )
    if( !defined( $domain ) );

while( $line = <STDIN> )
{
    my ($username, $uid, $name);

    ($username, undef, $uid, undef, $name) = split( /:/, $line );
    if(
	 defined( $username ) && defined( $name ) && defined( $uid )
	 && $uid >= $uid_min
      )
    {
	$name =~ s/,.*//;

	if( $name eq "" )
	{
	    print( "alias $username $username\@$domain\n" )
	}
	else
	{
	    print( "alias $username $name <$username\@$domain>\n" )
	}
    }
}

print( "# vim" . ": set ft=muttrc:\n" );
