#! /usr/bin/perl -w
# Created	: Wed 12 Apr 2006 06:52:13 PM CDT
# Modified	: Tue 09 May 2006 05:54:49 PM CDT
# Author	: Gautam Iyer <gautam@math.uchicago.edu>

# POD documentation {{{

=head1 NAME

pscenter.pl - Center PS files with different odd / even side margins.

=head1 SYNOPSIS

B<pscenter.pl> [I<options>] [F<infile> [F<outfile>]] [I<-- pstops_options>]

B<pscenter.pl> I<--help>

=head1 DESCRIPTION

Takes PS files which have a different left margin for odd and even pages, and generates a PS file with the same left margin for all pages. Optionally if you specify the right margin for the first page, then the output is centered (assuming of course that the odd and even pages have the same text width).

This script is a front end for pstops(1), supplied with psutils. You can obtain psutils from L<http://www.tardis.ed.ac.uk/~ajcd/psutils>.

=head1 OPTIONS

=over

=item I<-l> B<margin>, I<--left-margin> B<margin>

Specify the left margin of both the even and odd pages. B<margin> is in PostScript points, but can be suffixed with "in" or "cm" to make it in inches or centimeters respectively.

=item I<-r> B<margin>, I<--right-margin> B<margin>

Specify the right margin of the first page. If specified, the output file will also be centered.

=item I<-o> B<margin>, I<--odd-margin> B<margin>

=item I<-e> B<margin>, I<--even-margin> B<margin>

Specify the odd and even page left margins respectively.

=item I<-N>

Dryrun. Don't do anything, and only print the pstops command.

=item I<--help>

Print a brief summary of options.

=back

=head1 SEE ALSO

mkbooklet.pl, pstops(1), psbook(1), psnup(1)

=head1 AUTHOR

Gautam Iyer <gautam@math.uchicago.edu>

=cut

# END POD DOCUMENTATION }}}

# ---------------------------------------------------------------------------- #
#
#				BEGIN PERL CODE
#
# ---------------------------------------------------------------------------- #

use strict;
use Getopt::Long qw(:config no_ignore_case bundling);
use Term::ANSIColor qw(:constants);

# ---------------------------------------------------------------------------- #
#
#				   PROTOTYPES
#
# ---------------------------------------------------------------------------- #
sub print_usage();
sub toPsPoints;

# ---------------------------------------------------------------------------- #
#
#			    BEGIN PROGRAM EXECUTION
#
# ---------------------------------------------------------------------------- #

# Terminal colors
my ($BD,$UL,$IT,$ER,$RE)=(RESET.CYAN,RESET.GREEN,RESET.YELLOW,RESET.RED,RESET);

my $oddMargin	= "2cm";
my $evenMargin	= "2cm";
my $leftMargin	= undef;
my $rightMargin = undef;
my $print_help	= 0;
my $dryrun	= 0;

my ($x1, $x2);

GetOptions(
    "odd-margin|o=s"	=> \$oddMargin,
    "even-margin|e=s"	=> \$evenMargin,
    "left-margin|l=s"	=> \$leftMargin,
    "right-margin|r=s"	=> \$rightMargin,
    "dry-run|N"		=> \$dryrun,
    "help"		=> \$print_help
) or print_usage();

if( defined( $leftMargin ) )
{
    $oddMargin	= $leftMargin;
    $evenMargin	= $leftMargin;
}

# Convert all units to post script points
toPsPoints( $oddMargin, $evenMargin, $rightMargin );

print_usage() if( $print_help );

if ( defined( $rightMargin) )
{
    my $leftMargin = ($oddMargin + $rightMargin) / 2.0;
    
    $x1 = $leftMargin - $oddMargin;
    $x2 = $leftMargin - $evenMargin;
}
else
{
    $x1 = ($evenMargin - $oddMargin) / 2.0;
    $x2 = ($oddMargin - $evenMargin) / 2.0;
}


if( $dryrun )
{
    print "pstops '2:0(${x1},0),1(${x2},0)' @ARGV\n";
}
else
{
    exec "pstops '2:0(${x1},0),1(${x2},0)' @ARGV\n";
};

# ---------------------------------------------------------------------------- #
#
#			       BEGIN SUBROUTINES
#
# ---------------------------------------------------------------------------- #
sub print_usage()
{
    print << "EOF";
Usage: ${BD}pscenter.pl${RE} [${UL}options${RE}] [${IT}input.ps output.ps${RE}]

Takes a file with different even / odd page margins, and produces an
(optionally centered) file with equal margins on even and odd pages. Use
${UL}perldoc pscenter.pl${RE} for the complete documentation.

${BD}OPTIONS$RE

    ${UL}-l ${IT}margin${RE}	Left margin of both odd and even pages.
    ${UL}-r ${IT}margin${RE}	Right margin of the first page
    ${UL}-o ${IT}margin${RE}	Left margin of odd pages
    ${UL}-e ${IT}margin${RE}	Right margin of even pages
    ${UL}-N${RE}		Dry run. Just print the pstops command.
EOF

    exit 1;
}

sub toPsPoints()
{
    my $number = qr/[+-]?(?:\d+|\d*(?:\.\d+))/o;

    FOR: for (@_)
    {
	if( !defined( $_ ) )
	{
	    next FOR;
	};

	m/^(${number})in/ && do
	{
	    $_ = 72 * $1;
	    next FOR;
	};

	m/^(${number})cm$/ && do
	{
	    $_ = (72 / 2.54) * $1;
	    next FOR;
	};
    }
}
