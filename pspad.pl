#! /usr/bin/perl -w
# Created   : Tue 19 Feb 2008 01:37:25 AM PST
# Modified  : Tue 19 Feb 2008 01:37:25 AM PST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
# 
# Pad a PS file by shrinking it and adding a margin

use strict;
use Getopt::Long qw(:config no_ignore_case bundling);

GetOptions(
    "margin|m=s"	=> \$oddMargin,
    "src-paper|P=s"	=> \$evenMargin,
    "dst-paper|p=s"	=> \$leftMargin,
    "help"		=> \$print_help
) or print_usage();


sub toPsPoints()
{
    my $number = qr/[+-]?(?:\d+|\d*(?:\.\d+))/o;

    FOR: for (@_)
    {
	if( !defined( $_ ) )
	{
	    next FOR;
	};

	m/^(${number})in/ && do
	{
	    $_ = 72 * $1;
	    next FOR;
	};

	m/^(${number})cm$/ && do
	{
	    $_ = (72 / 2.54) * $1;
	    next FOR;
	};
    }
}

