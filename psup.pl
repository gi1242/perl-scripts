#! /usr/bin/perl -w
# Generates options for pstops to 2up your paper
# Author	: Gautam Iyer
# Created	: Mon 03 Oct 2005 11:22:20 PM CDT
# Modified	: Fri 21 Oct 2005 09:32:28 PM CDT

use strict;
use Getopt::Long qw(:config no_ignore_case bundling);
use Term::ANSIColor qw(:constants);

sub print_help;
sub get_dims;

# Terminal colors
my ($BD, $UL, $IT, $RE) = ( RESET.CYAN, RESET.GREEN, RESET.YELLOW, RESET);

my ($swidth, $sheight) = get_dims( "letter");	# Default to US letter
my ($dwidth, $dheight);
my $format = "duplex2up";

# Default tex margins (for letter paper) are
# left/right:1.77in, top: ~1.65 (to title), ~1.35 to writing, bottom: ~1.3
my ($topmargin, $botmargin) = (2.75, 2.25);
my $symmargin;
my $print_help = 0;
my $dryrun = 0;
my $srcpaper;
my $dstpaper = "letter";

GetOptions(
    "src-paper|p=s"	=> \$srcpaper,
    "src-height|h=f"	=> \$sheight,	# Paper height (cm)
    "src-width|w=f"	=> \$swidth,	# Paper width  (cm)
    "dst-paper|P=s"	=> \$dstpaper,
    # "dst-height|H=f"	=> \$dheight,	# Paper height (cm)
    # "dst-width|W=f"	=> \$dwidth,	# Paper width  (cm)
    "format|f=s"	=> \$format,	# "book", "duplex2up", "2up"
    "top-margin|t=f"	=> \$topmargin,	# Margin you want to crop from
    "bottom-margin|b=f"	=> \$botmargin,	# Bottom margin (if different)
    "margin|m=f"	=> \$symmargin, # Symmetric margins
    "dryrun|N"		=> \$dryrun,
    "help"		=> \$print_help
) or print_help();

print_help() if( $print_help);

# Get sourc and destination paper dimensions
($swidth, $sheight) = get_dims( $srcpaper) if( defined( $srcpaper));
($dwidth, $dheight) = get_dims( $dstpaper);

# Dimensions after rotating and scaling the image
my ($scaled_height, $scaled_width)   = ($dwidth, $dheight / 2);

# Compute dimensions of the cropped page
$topmargin = $botmargin = $symmargin if( defined( $symmargin));
# $botmargin = $topmargin unless( defined( $botmargin));
my $newheight = $sheight - $topmargin - $botmargin;
my $newwidth  = ($newheight / $scaled_height) * $scaled_width;

my $leftmargin = ($swidth - $newwidth)/2;
print STDERR RED, "Left margin negative\n", RESET if( $leftmargin < 0);

# Compute shrink factor
my $factor = $scaled_height / $newheight;

# Compute page offsets
my $xoffset1 = $dwidth + $botmargin * $factor;
my $xoffset2 = $xoffset1;

my $yoffset1 = -$leftmargin * $factor;
my $yoffset2 = $dheight/2 + $yoffset1;

my $yoffset3 = ($newwidth + $leftmargin) * $factor;
my $yoffset4 = $dheight/2 + $yoffset3;

my $xoffset3 = -$botmargin*$factor;
my $xoffset4 = $xoffset3;

# Build the pstops command line
my $paperopt = ( defined( $dstpaper) ? "-p$dstpaper" : "");
my $sizeopt;

if( $format eq "2up" ) {
    # 2up single sided printing
    $sizeopt = "\"2:0\@${factor}L(${xoffset1}cm,${yoffset1}cm)+1\@${factor}L(${xoffset2}cm,${yoffset2}cm)\"";
} elsif ($format eq "book") {
    # Booklet printing. This doesnt' work too well. psbook works better.
    $sizeopt = "\"4:-3\@${factor}L(${xoffset1}cm,${yoffset1}cm)+0\@${factor}L(${xoffset2}cm,${yoffset2}cm),-2\@${factor}R(${xoffset3}cm,${yoffset3}cm)+1\@${factor}R(${xoffset4}cm,${yoffset4}cm)\"";
    # print "pstops -pletter \"4:-3\@${factor}L(${xoffset1}in,${yoffset1}in)+0\@${factor}L(${xoffset2}in,${yoffset2}in)\" @ARGV\n";
} elsif ($format eq "duplex2up") {
    # 2up duplex printing
    $sizeopt = "\"4:0\@${factor}L(${xoffset1}cm,${yoffset1}cm)+1\@${factor}L(${xoffset2}cm,${yoffset2}cm),3\@${factor}R(${xoffset3}cm,${yoffset3}cm)+2\@${factor}R(${xoffset4}cm,${yoffset4}cm)\"";
} else {
    die "Unrecognized format $format. Format should be 2up, duplex2up or book\n";
};

# Call pstops and exit
if( $dryrun) {
    print "pstops $paperopt $sizeopt\n";
} else {
    exec "pstops $paperopt $sizeopt";
};

# tocm {{{1
sub tocm {
    return $_[0] * 2.54;
}

# Getdims {{{1
sub get_dims {
    $_ = shift;

    if( $_ eq "letter") {
	return ( tocm( 8.5), tocm( 11) );
    } elsif( $_ eq "legal") {
	return ( tocm( 8.5), tocm( 14) );
    } elsif( $_ eq "tabloid") {
	return ( tocm( 11), tocm( 17) );
    } elsif( $_ eq "a3" ) {
	return ( 29.7, 42);
    } elsif( $_ eq "a4" ) {
	return (  21, 29.7 );
    } elsif( $_ eq "a5" ) {
	return ( 14.8, 21);
    # } elsif( $_ eq "b4" ) {
    #     return ( 25.7, 36.4);
    } elsif( $_ eq "b5" ) {
	return ( 18.2, 25.7);
    } else {
	die "Unsupported paper type";
    }
}

# Print help{{{1
sub print_help {
    print << "EOF";
${BD}psup.pl${RE} [options] < input.ps > output.ps

${BD}psup.pl$RE takes a postscript file (stdin), and 2up's it (i.e. rotates and puts
two pages on the same page). It is similar to psnup, except it cuts out the
margins making the end result have larger, more readable text. For example, to
generate little booklets use

    psbook < input.ps | psup.pl > output.ps

This script is a front end for ${BD}pstops$RE, supplied with ${BD}psutils$RE.
You can obtain ${BD}psutils$RE from ${IT}http://www.tardis.ed.ac.uk/~ajcd/psutils$RE.

${BD}OPTIONS$RE
    $UL-m$RE, $UL--margin ${IT}margin$RE
	Specify the top and bottom margins (cm) to clip. Based on this the left
	and right margins are computed (keeping the aspect ratio fixed). The
	remaining visible area is then shrunk and rotated to produce your 2uped
	ps file. You can also specify the top and bottom margins differently
	using $UL-t$RE and $UL-b$RE. The default is to use asymetric margins.

    $UL-t$RE, $UL--top-margin ${IT}margin$RE
    $UL-b$RE, $UL--bottom-margin ${IT}margin$RE
	Specify the top/bottom margins. Default 2.75cm (top) and 2.25cm
	(bottom).

    $UL-h$RE, $UL--src-height ${IT}height$RE
    $UL-w$RE, $UL--src-width ${IT}width$RE
	Speficy source paper ${RE}height$RE or ${RE}width in cm.

    $UL-p$RE, $UL--src-paper$RE ${IT}paper$RE
    $UL-P$RE, $UL--dst-paper$RE ${IT}paper$RE
	Specify the paper type of the source / destination. Paper can be one of
	${BD}letter${RE} (default), ${BD}a3$RE, ${BD}a4$RE, ${BD}a5$RE or ${BD}b5$RE

    $UL-f$RE, $UL--format ${IT}format$RE
	Speficy the format:
	    ${BD}2up$RE		: 2up pages for non-duplex printing
	    ${BD}duplex2up$RE	: 2up pages for duplex printing (default)
	    ${BD}book$RE	: Booklet like pages (working badly)

	Rather than use "-f book", use

	    psbook < file.ps | psup.pl | lpr

    $UL-N$RE	Dryrun. Just print the pstops command.
EOF

    exit
}
