#! /usr/bin/perl -w
# Delete all files in tmp directories which are older than 7 days

use strict;
use POSIX qw(strftime);

my ($result, $file, $list);

# Get list of files which are older than 7 days
$list = `/usr/bin/find ~/tmp/ ~/local/tmp/ -ctime +7 -maxdepth 1 -print`;

while($list =~ m/^.+$/mg) {
    $file = $&;
    next if($file =~ m/tmp\/?$/); # Do not delete tmp directories

    $result = system("/bin/rm", "-rf", $file);

    if($result) {
	print STDERR strftime('%c', localtime()), " Error deleting $file\n";
    } else {
	print strftime('%a %b %d %H:%M', localtime()), " $file\n";
    }
}
