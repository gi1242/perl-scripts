#! /usr/bin/perl -w
# Created   : Sat 23 Oct 2010 01:31:11 AM EDT
# Modified  : Sun 31 Jul 2016 04:37:57 PM EDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>

use strict;

die( "No rotation argument\n" ) if( @ARGV == 0 );

my $orientation = $ARGV[0];
my ($LVDS, $VGA) = ($ENV{XRANDR_LVDS}, $ENV{XRANDR_VGA});
my (@devices, @inputs) = ();
my $dev;
my ($xrandr_rotation, $wacom_rotation, @xinput_rotation);
my $tablet_rotation;

if( $orientation =~ m/^(:?inverted|half$)/io )
{
    ($xrandr_rotation, $wacom_rotation) = qw(inverted half);
    @xinput_rotation = (-1, 0, 1, 0, -1, 1, 0, 0, 1 );
}
elsif( $orientation =~ m/^(:?normal|NONE$)/io )
{
    ($xrandr_rotation, $wacom_rotation) = qw(normal NONE);
    @xinput_rotation = (1, 0, 0, 0, 1, 0, 0, 0, 1);
}
elsif( $orientation =~ m/^(:?left|ccw$)/io )
{
    ($xrandr_rotation, $wacom_rotation) = qw(left ccw);
    @xinput_rotation = (0, -1, 1, 1, 0, 0, 0, 0, 1);
}
elsif( $orientation =~ m/^(:?right|cw$)/io )
{
    ($xrandr_rotation, $wacom_rotation) = qw(right cw);
    @xinput_rotation = (0, 1, 0, -1, 0, 1, 0, 0, 1 );
}
else
{
    die( "Must specify rotation (normal, inverted, left, right)\n" );
}

# Get the wacom devices.
for $dev (keys( %ENV ))
{
    next unless( $dev =~ m/^WACOM_/ );

    push( @devices, $ENV{$dev} );
}

if ( @devices == 0 )
{
    # Now try from xsetwacom
    #@devices = split( /[ \t]+.*\n/m, `xsetwacom list dev` );
    @devices = split(
	/\s+(?:id:\s.*?)?(?:STYLUS|ERASER|CURSOR|PAD|TOUCH|FINGER)\s*\n/im,
	`xsetwacom list dev` );
}

if ( @devices == 0 )
{
    # Give up.
    die( "Could not get wacom devices from environment/xsetwacom\n" )
}

#Rotate the display
system( qw(xrandr --dpi), $ENV{XRANDR_DPI}, '--output', $LVDS,
    qw(--auto --rotate), $xrandr_rotation, qw(--output), $VGA, qw(--off));

# Rotate tablet
my $old_rotation = `xsetwacom get "$devices[0]" Rotate`;
for $dev (@devices)
{
    system( qw(xsetwacom set), $dev, 'Rotate', $wacom_rotation );
}

# 2016-07-31: After kernel upgrade the FINGER device wasn't managed by
# xsetwacom anymore. Rotate it via xinput. Rotate finger devices that are not
# managed by xsetwacom
@inputs = grep { chomp(); m/wacom/i && !is_contained($_, @devices) }
    qx(xinput --list --name-only);
for $dev (@inputs)
{
    #print( "$dev\n" );
    system( qw(xinput --set-prop), $dev, 'Coordinate Transformation Matrix',
	@xinput_rotation );
}

# Set calibration parameters
$tablet_rotation = `xsetwacom get "$devices[0]" Rotate`;
chomp( $tablet_rotation );
if( -x "$ENV{HOME}/.calibrate.${tablet_rotation}" )
{
    print( "Executing $ENV{HOME}/.calibrate.${tablet_rotation}\n" );
    system( "$ENV{HOME}/.calibrate.${tablet_rotation}" );
}

# Restart FVWM
if (
    ( $old_rotation =~ m/^(?:NONE|half)$/i
	&& $tablet_rotation =~ m/^(?:NONE|half)$/i )
    || ( $old_rotation =~ m/^(?:ccw|cw)$/i
	&& $tablet_rotation =~ m/^(?:ccw|cw)$/i )
   )
{
    if( lc($tablet_rotation) eq 'half' )
    {
	system( 'FvwmCommand', 'read edgebuttons.fvwm' );
	system( 'FvwmCommand', 'EdgeCommand west ToggleButtons' );
    }
    elsif( lc($tablet_rotation) eq 'none' )
    {
	system( 'FvwmCommand', 'EdgeCommand west Nop' );
    }
}
else
{
    system( qw(FvwmCommand Restart) );
}

sub is_contained
{
    my $e = shift();
    foreach my $i (@_)
    {
	return 1 if( $e eq $i );
    }
    return 0;
}
