#! /usr/bin/perl -w
# Created   : Fri 10 May 2019 09:10:02 PM EDT
# Modified  : Sun 12 May 2019 01:56:47 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'

# Find all symlinks and add them to seafile-ignore.txt

use strict;
use I18N::Langinfo qw(langinfo CODESET);
use File::Find;
use File::Slurper qw(read_text write_text);
use File::Spec::Functions qw{abs2rel splitpath catpath};
use List::Util qw(uniq);
use Try::Tiny;
use Term::ANSIColor qw(:constants);

my ($BD,$UL,$IT,$ER,$RE)=(RESET.CYAN,RESET.GREEN,RESET.YELLOW,RESET.RED,RESET);

my $lib_dir = $ARGV[0];
my (%links, %pholders);


die("Usage: seaf-link <libdir>\n") unless(@ARGV);

# Traverse directory
chdir($lib_dir) or die( "Error changing to $lib_dir: $!\n" );
find( { wanted => \&wanted, no_chdir => 1}, '.' );

if( create_placeholders() )
{
    print( << '	EOF' =~ s/^\t//gmr );
	Some links were removed, and placeholders were created in their place.
	Let seafile sync so that the files are removed on the server, and then
	re-run this command to update the ignore file and re-create the
	symlinks.
	EOF
}
elsif( update_ignore() )
{
    print( << "	EOF" =~ s/^\t//gmr );
	Some entries were added to seafile_ignore.txt. Let seafile sync, and
	then rerun this command to create symlinks.
	EOF
}
elsif( create_symlinks() )
{
    print( "Some conflicts were found. Fix them and re-run.\n" );
}

# All done.




#
# Subroutines
#
sub debug #{{{1
{
    warn( $ER, @_, "$RE\n" );
}

sub wanted #{{{1
{
    if( -l )
    {
	my $fname = abs2rel($_);
	$links{$fname} = readlink($fname);
    }
    elsif( abs2rel($_, $File::Find::dir) =~ m/^\.(.*)\.seaf-link$/ )
    {
	my $link_name = abs2rel( catpath( '', $File::Find::dir, "$1" ) );

	# Glib assumes all file names are UTF8
	$pholders{$link_name}
	    = read_text($_, 'UTF-8');
    }
}
sub create_placeholders #{{{1
{
    my $created_placeholder = 0;

    while( my ($link, $target) = each(%links) )
    {
	if( !exists( $pholders{$link} ) )
	{
	    # Create placeholder, remove symlink, ask for rerun after sync
	    my ($vol, $dirs, $fname) = splitpath($link);
	    my $ph_name =  catpath( $vol, $dirs, ".$fname.seaf-link" );

	    try
	    {
		# Glib assumes file names are UTF-8
		write_text( $ph_name, $target, 'UTF-8' );
		unlink($link) or die( $! );
	    }
	    catch
	    {
		warn( "${ER}Error: $_${RE}\n" );
	    };
	    $created_placeholder++;
	}
    }
    return $created_placeholder;
}

sub update_ignore #{{{1
{
    my $ig_fname = "seafile-ignore.txt";
    my $start_marker = "### BEGIN seaf-link generated list ###";
    my $end_marker = ($start_marker =~ s/BEGIN/END/r);

    my $ig_text = "$start_marker\n"
	. join( "\n", sort( uniq( keys(%links), keys(%pholders) ) ) )
	. "\n$end_marker\n";

    # Read/write ignore file with default encoding. (Presumably seafile client
    # does the same.)
    my $encoding = langinfo( CODESET() );
    my $ig_file_contents = read_text( $ig_fname, $encoding  );
    my $ig_file_new = '';

    if( $ig_file_contents =~ m/^$start_marker\n.*^$end_marker\n$/ms )
    {
	#debug( "\$&:\n$&\n\$ig_text:\n$ig_text" );
	$ig_file_new = $`.$ig_text.$'
	    if( $ig_text ne $& );
    }
    elsif( $ig_text ne "\n" )
    {
	$ig_file_new = $ig_file_contents . $ig_text;
    }

    if( $ig_file_new )
    {
	write_text( $ig_fname, $ig_file_new, $encoding );
	return 1;
    }
    else
    {
	return 0;
    }
}

sub create_symlinks #{{{1
{
    my $has_conflicts = 0;

    while( my ($link, $target) = each(%pholders) )
    {
	# Create symlink if possible
	if( -l $link )
	{
	    if( !exists( $links{$link} ) )
	    {
		warn( "${ER}Possible race condition with $link. Rerun$RE\n" );
		$has_conflicts = 1;
	    }
	    elsif( $links{$link} ne $target )
	    {
		print( "Skipping $link. Conflict: ",
		    "$UL$links{$link}$RE vs $IT$target$RE\n" );
		$has_conflicts = 1;
	    }
	    # Else all good.
	}
	elsif( ! -e $link  )
	{
	    symlink( $target, $link )
		or warn( "${IT}Error creating $link -> $target$RE\n" );
	}
	else
	{
	    warn( "${IT}Conflict: $link exists. Not linking to $target$RE\n" );
	    $has_conflicts = 1;
	}
    }

    return $has_conflicts;
}
