#! /usr/bin/perl -w
# a fast version of grep -vf file1 file2 assuming file1 & 2 are sorted.
# Print all line that occur in srcfile, but not in dstfile

use strict;
use Getopt::Long qw(:config no_ignore_case bundling);
use Term::ANSIColor qw(:constants);

my ($BD, $UL, $RE) = (CYAN, GREEN, RESET);

my ($dstline, $srcline);
my ($debug, $help);

GetOptions(
    "help|h"	=> \$help,
    "debug|d"	=> \$debug
) or $help=1;

# print( @ARGV);
if( $#ARGV != 1 || defined( $help)) {
    print << "EOF";
${BD}sortdiff.pl${RE} A fast version of "grep -vf dst src" assuming the input files
are sorted. Print all line that occur in src, but not in dst.

${BD}USAGE${RE}

    ${BD}sortdiff.pl${RE} [$UL-hd$RE] ${UL}src dst$RE

    ${UL}-h${RE}  Print this help
    ${UL}-d${RE}  Print some useless debug info.
EOF

    exit
}

# Uncomment for debug info
# $debug = 1;

open( SRC, "<$ARGV[0]") or die("Unable to open ${UL}$ARGV[0]$RE\n");
open( DST, "<$ARGV[1]") or die("Unable to open ${UL}$ARGV[1]$RE\n");

$dstline = <DST>;
while ($srcline = <SRC>) {
    last unless( defined( $srcline));

    if( !defined( $dstline) ) {
	# Dstfile is done, just cat the source, and end
	do {
	    print $srcline;
	} while( $srcline = <SRC>);
	last;
    } elsif( $srcline lt $dstline) {
	# srcline does not apear in dstfile. keep reading srcfile till we
	# pass dstline
	do {
	    print $srcline;
	} while( defined($srcline = <SRC>) && ($srcline lt $dstline));
	redo
    } elsif( $srcline eq $dstline) {
	# srcline apears in dstfile. read next line in both files
	print STDERR "= $srcline" if( defined( $debug));
	$dstline = <DST>;
	next
    } else {
	# srcline might apear later in dstfile. read the next line of dstfile
	# only.
	do {
	    print STDERR "> $dstline" if( defined( $debug));
	} while( defined($dstline = <DST>) && ($srcline gt $dstline));
	redo
    }
}
