#! /usr/bin/perl -w
use strict;
use POSIX qw(strftime);

my ($result, $file, $list);

$list = `/usr/bin/find ~/tmp/ ~/local/tmp/ -ctime +8 -maxdepth 1 -print`;

while($list =~ m/^.+$/mg) {
    print "$& Hello \n";
}
