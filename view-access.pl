#! /usr/bin/perl -w
# Created   : Mon 03 Feb 2014 04:39:00 PM EST
# Modified  : Fri 31 Jan 2020 08:38:47 AM EST
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'
#
# Grep user-names / access times from apache server logs.

use strict;
use Getopt::Std;

my %opts;
unshift( @ARGV, "-h" ) unless( @ARGV );
if( !getopts( 'lh', \%opts ) or $opts{h} )
{
    print << "EOF" ;
$0: Check various things in the access log

    -l	Shibboleth logins
EOF
}

elsif( $opts{l} )
{
    unshift( @ARGV, "$ENV{HOME}/tmp/access_log" ) unless( @ARGV );
    while(<>)
    {
	my ($clientAddress,    $rfc1413,      $username, 
	    $localTime,         $httpRequest,  $statusCode, 
	    $bytesSentToClient, $referer,      $clientSoftware) =
	/^(\S+) (\S+) (\S+) \[(.+)\] \"(.+)\" (\S+) (\S+) \"(.*)\" \"(.*)\"/o;

	if( defined($username) && $username =~ m/\@andrew\.cmu\.edu$/o )
	{
	    my ($getPost, $uri, $junk) = split(' ', $httpRequest, 3);
	    my $file = '';
	    $file = $1
		if( $uri =~ m:/\~gautam/sj/\S+auth/(getgrades\.py|\S+?\.pdf):o );

	    printf( "%8s %18s %s\n", 
		$username =~ s/\@andrew\.cmu\.edu$//ro, $file, $localTime )
	}
    }
}
