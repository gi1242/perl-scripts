#! /usr/bin/perl -w
# Created   : Mon 01 Aug 2016 08:42:40 PM EDT
# Modified  : Mon 15 Aug 2016 10:49:09 AM EDT
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'

use strict;

if( !scalar(@ARGV) )
{
    die( "Usage: volume { +/-N | toggle-mute | toggle-mic-mute }\n" );
}
elsif( $ARGV[0] =~ m/^([+-]?\d+%?|toggle-mute)$/n )
{
    my @sinks = map { m/^(\d+)\s/ ? $1 : () }
	qx(pactl list sinks short);

    if( $ARGV[0] eq 'toggle-mute' )
    {
	foreach my $s (@sinks)
	{
	    system( qw(pactl -- set-sink-mute), $s, 'toggle' )
	}
    }
    else
    {
	foreach my $s (@sinks)
	{
	    system( qw(pactl -- set-sink-volume), $s, $ARGV[0] )
	}

	# Notify using osd_cat
	if( qx( pactl list sinks ) =~ m'^\s*Volume:.*/\s*(\d+)%\s*/'m )
	{
	    system( qw{pkill -f (^|/)osd_cat\s.*(Brightness|Volume)} );
	    if( ! fork() )
	    {
		exec( qw( osd_cat -b percentage -p bottom -d 2
		    -o 100 -i 100
		    --font -*-times-medium-r-*-*-34-*-*-*-*-*-*-* -P ), $1,
		    '-T', "Volume: $1%" );
		die( "Error forking osd_cat\n" );
	    }
	}
    }
}
elsif( $ARGV[0] eq 'toggle-mic-mute' )
{
    my @sources = map { m/^(\d+)\s/ ? $1 : () }
	qx(pactl list sources short);

    foreach my $s (@sources)
    {
	system( qw(pactl -- set-source-mute), $s, 'toggle' )
    }
}
else
{
    die( "Unrecognized argument\n" );
}
