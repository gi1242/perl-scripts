#! /usr/bin/perl -w

use strict;
use XML::Parser;
use Getopt::Long qw(:config no_ignore_case bundling);
use Term::ANSIColor qw(:constants);

sub print_help()
{
    print << "EOF";
xmlsearch.pl: Search an XML file
EOF
    exit 1;
}

my ($file, $print_help);
my xmltree = XML::Parser::new();

GetOptions(
    "file|f=s"	=> \$file,
    "help"	=> \$print_help
);

print_help()
    if( defined($print_help) || !defined($file) );

xmltree->parsefile( $file ) || die( "Error parsing $file.\n" );
print( xmltree );
